require 'test_helper'

class RestaurantsControllerTest < ActionDispatch::IntegrationTest
 include Devise::Test::IntegrationHelpers
  setup do
    @restaurant = restaurants(:restaurant_one)
    @update = {
      name:      "Example",
      location:  "Example Town"
      }
    @user = users(:user_one)

  end

  test "should get index" do
    get restaurant_index_url
    assert_response :success
  end

  test "should get new" do
    sign_in @user
    get new_restaurant_url
    assert_response :success
  end

  test "should create restaurant" do
    sign_in @user
    assert_difference('Restaurant.count') do
      post restaurants_url, params: { restaurant: @update }
    end

    assert_redirected_to restaurant_url(Restaurant.last)
  end

  test "should show restaurant" do
    sign_in @user
    get restaurant_url(@restaurant)
    assert_response :success
  end

  test "should update restaurant" do
    patch restaurant_url(@restaurant), params: { restaurant: @update }
    assert_redirected_to login_url
  end

  test "should add upvote" do
    sign_in @user
    patch upvote_url(@restaurant)
    @restaurant.reload
    assert_equal 2, Restaurant.upvotes(@restaurant.id)
  end

  test "should add downvote" do
    sign_in @user
    patch downvote_url(@restaurant)
    @restaurant.reload
    assert_equal 1, Restaurant.downvotes(@restaurant.id)
  end


  test "should find restaurant by name" do 
  	get restaurants_url(@restaurant), params: {search: "Corner" }
  	@restaurant.reload

  	assert_response :success
  	assert_equal 1, assigns[:restaurants].count
  	
  end

  test "should not find restaurant by name" do 
  	get restaurants_url(@restaurant), params: {search: "Square" }
  	@restaurant.reload

  	assert_response :success
  	assert_equal 0, assigns[:restaurants].count
  	
  end

  test "should find restaurant by location" do 
  	get restaurants_url(@restaurant), params: {search: "Asheville" }
  	@restaurant.reload
  	
  	assert_response :success
  	assert_equal 1, assigns[:restaurants].count
  	
  end

  test "should not find restaurant by location" do 
  	get restaurants_url(@restaurant), params: {search: "Roswell" }
  	@restaurant.reload

  	assert_response :success
  	assert_equal 0, assigns[:restaurants].count

  end

  test "should find restaurant by name and location" do 
  	get restaurants_url(@restaurant), params: {search: "Carrollton" }
  	@restaurant.reload
  	
  	assert_response :success
  	assert_equal 3, assigns[:restaurants].count
  end

  test "should not find restaurant by name and location" do 
  	get restaurants_url(@restaurant), params: {search: "Charleston" }
  	@restaurant.reload

  	assert_response :success
  	assert_equal 0, assigns[:restaurants].count
  	
  end

end
