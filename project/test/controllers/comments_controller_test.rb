require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
include Devise::Test::ControllerHelpers
	setup do
		@comment = comments(:one)
		@comments = []
			@restaurant = restaurants(:restaurant_one)
			sign_in users(:user_one)
		
	end

	test "should add comment" do
		assert_equal 0, @restaurant.comments.count
		@restaurant.comments << @comment
		assert_equal 1, @restaurant.comments.count
	end
end
