require 'test_helper'

class RestaurantTest < ActiveSupport::TestCase
  setup do
    @restaurant_one = restaurants(:restaurant_one)
    @restaurant_two = restaurants(:restaurant_two)
    @restaurant_three = restaurants(:restaurant_three)

  end

  test "should have one upvote and no downvotes" do
    assert_equal 1, Restaurant.upvotes(@restaurant_one.id)
    assert_equal 0, Restaurant.downvotes(@restaurant_one.id)
  end

  test "should have one downvote and no upvotes" do
    assert_equal 1, Restaurant.downvotes(@restaurant_two.id)
    assert_equal 0, Restaurant.upvotes(@restaurant_two.id)
  end

  test "should have mixed numbers of up and down votes" do
    assert_equal 1, Restaurant.upvotes(@restaurant_three.id)
    assert_equal 3, Restaurant.downvotes(@restaurant_three.id)
  end
end
