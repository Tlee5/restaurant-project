Rails.application.routes.draw do
  
  devise_for :users
  root 'restaurants#index', as: 'restaurant_index'
  resources :restaurants, except: [:edit, :destroy]
  resources :votes, except: [:edit, :destroy]
  resources :comments
  patch '/restaurants/:id/upvote' => 'restaurants#upvote', as: :upvote
  patch '/restaurants/:id/downvote' => 'restaurants#downvote', as: :downvote
  get '/users/sign_in' => 'devise/sessions#new', as: :login
  get '/users/sign_out' => 'devise/sessions#destroy', as: :logout

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
