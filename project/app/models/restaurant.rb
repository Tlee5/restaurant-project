class Restaurant < ApplicationRecord
 validates :name, :location, presence: true
 validates :name, uniqueness: true
 has_many :votes
 has_many :comments

 def self.upvotes(id)
   votes = Vote.all
   sum = 0
   votes.each do |vote|
     if id == vote.restaurant_id and vote.voted == true
       sum += 1
     end
   end
  return sum
 end

 def self.downvotes(id)
   votes = Vote.all
   sum = 0
   votes.each do |vote|
     if id == vote.restaurant_id and vote.voted == false
       sum += 1
     end
   end
  return sum
 end

  def self.search(search)
  where("name LIKE ? OR location LIKE ?", "%#{search}%", "%#{search}%") 
  end

end
