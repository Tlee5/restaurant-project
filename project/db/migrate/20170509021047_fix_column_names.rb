class FixColumnNames < ActiveRecord::Migration[5.0]
  def change
  	rename_column :comments, :restaurants_id, :restaurant_id
  	rename_column :comments, :users_id, :user_id
  end
end
