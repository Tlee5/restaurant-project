class RemoveUpvoteAndDownvoteFromRestaurants < ActiveRecord::Migration[5.0]
  def change
    remove_column :restaurants, :upvote, :integer
    remove_column :restaurants, :downvote, :integer
  end
end
