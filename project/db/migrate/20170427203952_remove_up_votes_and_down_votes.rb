class RemoveUpVotesAndDownVotes < ActiveRecord::Migration[5.0]
  def change
    remove_column :restaurants, :up_votes, :integer
    remove_column :restaurants, :down_votes, :integer
  end
end
