# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Highland deli
Restaurant.create(name: 'Highland Deli', location: 'Carrollton, GA', up_votes: 4, down_votes: 3)

#Corner Cafe
Restaurant.create(name: 'Corner Cafe', location: 'Carrollton, GA', up_votes: 5, down_votes: 3)

#Bone Gardne
Restaurant.create(name: 'Bone Garden', location: 'Atlanta, GA', up_votes: 8, down_votes: 2)

#Tupeol
Restaurant.create(name: 'Tupelo', location: 'Asheville, NC', up_votes: 1, down_votes: 8)

#Harvey's
Restaurant.create(name: "Harvey's", location: 'Detroit, MI', up_votes: 7, down_votes: 4)

#Heirloom Cafe
Restaurant.create(name: 'Heirloom Cafe', location: 'Athens, GA', up_votes: 9, down_votes: 5)

#Gumbeaux
Restaurant.create(name: 'Gumbeaux', location: 'Douglasville, GA', up_votes: 3, down_votes: 0)

#Boar's Head Grill & Tavern
Restaurant.create(name: "Boar's Head Grill & Tavern", location: 'Savannah, GA', up_votes: 0, down_votes: 1)

#Panera
Restaurant.create(name: 'Panera', location: 'Carrollton, GA', up_votes: 11, down_votes: 1)

#Front Page News
Restaurant.create(name: 'Front Page News', location: 'Atlanta, GA', up_votes: 9, down_votes: 4)
